var MenuConfig = [{
    type: 'file',
    name: '文件',
    children: [{
        name: '新建',
        func: 'onFileNew'
    }, {
        name: '打开',
        func: 'onFileOpen'
    }, {
        name: '保存',
        func: 'onFileSave'
    }]
},{
    type: 'desktop',
    name: '运行',
    children: [{
        name: '开电源',
        func: 'onPowerOn'
    }, {
        name: '关电源',
        func: 'onPowerOff'
    }, {
        name: '重启',
        func: 'onReset'
    }]
},{
    type: 'setting',
    name: '工具',
    children: [{
        name: '存储器芯片设置',
        func: 'onSettingChip'
    }, {
        name: '连接线颜色设置',
        func: 'onSettingLineColor'
    }, {
        name: '时钟周期设置',
        func: 'onSettingClock'
    }]
},{
    type: 'experiment',
    name: '实验指导',
    children: [{
        name: '实验指导书',
        func: 'onOpenDocument'
    }, {
        name: '实验器材资料',
        func: 'onOpenData'
    }]
},{
    type: 'question-circle',
    name: '帮助',
    children: [{
        name: '查看帮助',
        func: 'onViewHelp'
    }, {
        name: '关于虚拟实验室',
        func: 'onViewVirtualLab'
    }]
}]

var ChipsConfig = [{
    type: 'file',
    name: '数字功能器件',
    children: [{
        name: '74LS181',
        func: 'onFileNew'
    }, {
        name: '74LS245',
        func: 'onFileOpen'
    }, {
        name: '74LS175',
        func: 'onFileSave'
    }, {
        name: '74LS174',
        func: 'onFileSave'
    }, {
        name: '74LS273',
        func: 'onFileSave'
    }, {
        name: '74LS139',
        func: 'onFileSave'
    }, {
        name: '74LS374',
        func: 'onFileSave'
    }, {
        name: '74LS163',
        func: 'onFileSave'
    }, {
        name: 'RAM6116',
        func: 'onFileSave'
    }]
},{
    type: 'desktop',
    name: '逻辑电路',
    children: [{
        name: 'ANDgate',
        func: 'onPowerOn'
    }, {
        name: 'ORgate',
        func: 'onPowerOff'
    }, {
        name: 'NOTgate',
        func: 'onRestart'
    }, {
        name: 'NANDgate',
        func: 'onRestart'
    }, {
        name: 'XORgate',
        func: 'onRestart'
    }, {
        name: 'Triplegate',
        func: 'onRestart'
    }]
},{
    type: 'experiment',
    name: '基本元件',
    children: [{
        name: 'Switch',
        func: 'onOpenInstruction'
    }, {
        name: 'LED',
        func: 'onOpenData'
    }, {
        name: 'SinglePluse',
        func: 'onOpenData'
    }, {
        name: 'ContinuousPulse',
        func: 'onOpenData'
    }]
},{
    type: 'question-circle',
    name: '虚拟组件',
    children: [{
        name: 'EPROM2716C3',
        func: 'onViewHelp'
    }, {
        name: 'EPROM2716C4',
        func: 'onViewVirtualLab'
    }, {
        name: 'SequeTimer',
        func: 'onViewVirtualLab'
    }, {
        name: 'BUS',
        func: 'onViewVirtualLab'
    }]
}]
